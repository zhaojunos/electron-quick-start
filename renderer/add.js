const { ipcRenderer } = require('electron')
const { $ } = require('./helper')
const path = require('path')

let musicFilePah = []

$('select-music-btn').addEventListener('click', () => {
    ipcRenderer.send('open-music-file')
})

$('export-music-btn').addEventListener('click', () => {
    if (musicFilePah.length != 0) {
        ipcRenderer.send('add-tracks', musicFilePah)
    }
})

const renderListHTML = (filePathArrays) => {
    const musicList = $('musicList')
    const musicItemsHTML = filePathArrays.reduce((html, music) => {
        html += `<li class="list-group-item"><i class="fa fa-music mr-2"></i>${path.basename(music)}</li>`
        return html
    }, '')
    musicList.innerHTML = `<ul class="list-group">${musicItemsHTML}</ul>`
}


ipcRenderer.on('selected-file', (event, path) => {
    if ((filePathArrays = path.filePaths)) {
        if (Array.isArray(filePathArrays)) {
            renderListHTML(filePathArrays)
            musicFilePah = filePathArrays
        }
    }
})