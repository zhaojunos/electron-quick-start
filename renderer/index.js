const { ipcRenderer, ipcMain } = require('electron')
const { $, convertDuration } = require('./helper')

let musicAudio = new Audio()
let allTracks
let currentTrack

$('add-music-btn').addEventListener('click', () => {
    ipcRenderer.send('add-music')
})

const renderPlayerHTML = (name, duiation) => {
    const palyer = $('player-status')
    const html = `<div class="col font-weight-bold">
                正在播放：${name}
                </div>
                <div class="col">
                    <span id="current-seeker">00:00</span> / ${convertDuration(duiation)}
                </div>`
    palyer.innerHTML = html
}

const updateProgressHTML = (currentTime, duiation) => {

    const seeker = $('current-seeker')
    const progress = $('progress-bar')
    seeker.innerHTML = convertDuration(currentTime)
    progressData = Math.floor(currentTime / duiation * 100)
    progress.innerHTML = progressData + '%'
    progress.style.width = progressData + '%'
}


ipcRenderer.on('getTracks', (event, tracks) => {
    allTracks = tracks
    renderListHTML(tracks)
})

const renderListHTML = (tracks) => {
    const tracksList = $('tracksList')
    const tracksItemsHTML = tracks.reduce((html, track) => {
        html += `<li class="row music-track list-group-item d-flex justify-content-between align-item-center">
            <div class="col-10">
                <i class="fa fa-music mr-2"></i>
                <b>${track.filename}</b>
            </div>
            <div class="col-2">
                <i class="fa fa-play mr-4" data-id="${track.id}"></i>
                <i class="fa fa-trash" data-id="${track.id}"></i>
            </div>

        </li>`
        return html
    }, '')
    const emptyTrackHTML = '<div class="alert alert-primary">没有音乐</div>'
    tracksList.innerHTML = tracks.length ? `<ul class="list-group">${tracksItemsHTML}</ul>` : emptyTrackHTML
}

musicAudio.addEventListener('loadedmetadata', () => {
    renderPlayerHTML(currentTrack.filename, musicAudio.duration)
})

musicAudio.addEventListener('timeupdate', () => {
    updateProgressHTML(musicAudio.currentTime, musicAudio.duration)
})

$('tracksList').addEventListener('click', (event) => {
    event.preventDefault()
    const { dataset, classList } = event.target
    const id = dataset && dataset.id

    if (id && classList.contains('fa-play')) {
        if (currentTrack && currentTrack.id === id) {
            musicAudio.play()
        } else {
            currentTrack = allTracks.find(track => track.id === id)
            musicAudio.src = currentTrack.path
            musicAudio.play()
            const resetIconEle = document.querySelector('.fa-pause')
            if (resetIconEle) {
                resetIconEle.classList.replace('fa-pause', 'fa-play')
            }
        }
        classList.replace('fa-play', 'fa-pause')
    } else if (id && classList.contains('fa-pause')) {
        musicAudio.pause()
        classList.replace('fa-pause', 'fa-play')
    } else if (id && classList.contains('fa-trash')) {
        ipcRenderer.send('delete-track', id)
    }
})